#ifndef OSMO_BTS_MEAS_H
#define OSMO_BTS_MEAS_H

#define HO_RQD_CAUSE_MAX 17

#define HO_RQD_CAUSE_L_RXLEV_UL_H	(1 << 0)
#define HO_RQD_CAUSE_L_RXLEV_DL_H	(1 << 1)
#define HO_RQD_CAUSE_L_RXQUAL_UL_H	(1 << 2)
#define HO_RQD_CAUSE_L_RXQUAL_DL_H	(1 << 3)
#define HO_RQD_CAUSE_RXLEV_UL_IH	(1 << 4)
#define HO_RQD_CAUSE_RXLEV_DL_IH	(1 << 5)
#define HO_RQD_CAUSE_MAX_MS_RANGE	(1 << 6)
#define HO_RQD_CAUSE_POWER_BUDGET	(1 << 7)
#define HO_RQD_CAUSE_ENQUIRY		(1 << 8)
#define HO_RQD_CAUSE_ENQUIRY_FAILED	(1 << 9)
#define HO_RQD_CAUSE_NORMAL3G		(1 << 10)	/* currently unsupported */
#define HO_RQD_CAUSE_EMERGENCY3G	(1 << 11)	/* currently unsupported */
#define HO_RQD_CAUSE_SRV_PREFERRED3G	(1 << 12)	/* currently unsupported */
#define HO_RQD_CAUSE_O_M_SHUTDOWN	(1 << 13)
#define HO_RQD_CAUSE_QUALITY_PROMOTION	(1 << 14)
#define HO_RQD_CAUSE_LOAD_PROMOTION	(1 << 15)
#define HO_RQD_CAUSE_LOAD_DEMOTION	(1 << 16)

#define MEAS_PREPROC_AVG_PARAMID_RXLEV	(1 << 0)
#define MEAS_PREPROC_AVG_PARAMID_RXQUAL	(1 << 1)
#define MEAS_PREPROC_AVG_PARAMID_DIST	(1 << 2)
#define MEAS_PREPROC_SDCCH_HO_GSM		(1 << 3)
#define MEAS_PREPROC_SDCCH_HO_UMTS		(1 << 4)	/* currently unsupported */

#define MEAS_PREPROC_AVG_METHOD_UNWEIGHTED	(1 << 8)
#define MEAS_PREPROC_AVG_METHOD_WEIGHTED	(1 << 9)
#define MEAS_PREPROC_AVG_METHOD_MEDIAN	(1 << 10)

/*
 * HO Directed Retry Timer parameter may configurable
 * via HO threshold comparator EIE of  RSL IPAC MEAS_PREPROCE_DFLT message.
 * Directed Retry Timer parameter is harcoded for current implementation.
 */
#define MEAS_PREPROC_HO_DIR_TMR_DUR_MIN	1
#define MEAS_PREPROC_HO_DIR_TMR_DUR_MAX	3

static const struct value_string rsl_ipac_preproc_meas_ho_cause_strs[] = {
	{ IPAC_HO_RQD_CAUSE_L_RXLEV_UL_H, "IPAC_HO_RQD_CAUSE_L_RXLEV_UL_H" },
	{ IPAC_HO_RQD_CAUSE_L_RXLEV_DL_H, "IPAC_HO_RQD_CAUSE_L_RXLEV_DL_H" },
	{ IPAC_HO_RQD_CAUSE_L_RXQUAL_UL_H, "IPAC_HO_RQD_CAUSE_L_RXQUAL_UL_H" },
	{ IPAC_HO_RQD_CAUSE_L_RXQUAL_DL_H, "IPAC_HO_RQD_CAUSE_L_RXQUAL_DL_H" },
	{ IPAC_HO_RQD_CAUSE_RXLEV_UL_IH, "IPAC_HO_RQD_CAUSE_RXLEV_UL_IH" },
	{ IPAC_HO_RQD_CAUSE_RXLEV_DL_IH, "IPAC_HO_RQD_CAUSE_RXLEV_DL_IH" },
	{ IPAC_HO_RQD_CAUSE_MAX_MS_RANGE, "IPAC_HO_RQD_CAUSE_MAX_MS_RANGE" },
	{ IPAC_HO_RQD_CAUSE_POWER_BUDGET, "IPAC_HO_RQD_CAUSE_POWER_BUDGET" },
	{ IPAC_HO_RQD_CAUSE_ENQUIRY, "IPAC_HO_RQD_CAUSE_ENQUIRY" },
	{ IPAC_HO_RQD_CAUSE_ENQUIRY_FAILED, "IPAC_HO_RQD_CAUSE_ENQUIRY_FAILED" },
	{ 0, NULL }
};

#define MEAS_MAX_TIMING_ADVANCE 63
#define MEAS_MIN_TIMING_ADVANCE 0
#define MEAS_TA_DIST_STEP 550

int lchan_new_ul_meas(struct gsm_lchan *lchan, struct bts_ul_meas *ulm, uint32_t fn);

int lchan_meas_check_compute(struct gsm_lchan *lchan, uint32_t fn);

/* build the 3 byte RSL uplinke measurement IE content */
int lchan_build_rsl_ul_meas(struct gsm_lchan *, uint8_t *buf);

void meas_preproc_rep_init(struct gsm_lchan *lchan);
int meas_better_cell(struct gsm_lchan *lchan);
int meas_lookup_ncell_list(struct gsm_lchan *lchan);
int meas_update_ho_causes(struct gsm_lchan *lchan);
int meas_preproc_avg(struct gsm_lchan *lchan);
int meas_parse_ms_rep(struct msgb *msg, struct gsm_lchan *lchan);
int meas_update_record_ho_causes(struct gsm_lchan *lchan);


#endif
