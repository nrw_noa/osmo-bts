
#include <stdint.h>
#include <errno.h>

#include <osmocom/gsm/gsm_utils.h>

#include <osmo-bts/gsm_data.h>
#include <osmo-bts/logging.h>
#include <osmo-bts/measurement.h>
#include <osmo-bts/scheduler.h>
#include <osmo-bts/bts.h>

/* Tables as per TS 45.008 Section 8.3 */
static const uint8_t ts45008_83_tch_f[] = { 52, 53, 54, 55, 56, 57, 58, 59 };
static const uint8_t ts45008_83_tch_hs0[] = { 0, 2, 4, 6, 52, 54, 56, 58 };
static const uint8_t ts45008_83_tch_hs1[] = { 14, 16, 18, 29, 66, 68, 70, 72 };

/* find out if an array contains a given key as element */
#define ARRAY_CONTAINS(arr, val) array_contains(arr, ARRAY_SIZE(arr), val)
static bool array_contains(const uint8_t *arr, unsigned int len, uint8_t val) {
	int i;
	for (i = 0; i < len; i++) {
		if (arr[i] == val)
			return true;
	}
	return false;
}

/* Decide if a given frame number is part of the "-SUB" measurements (true) or not (false) */
static bool ts45008_83_is_sub(struct gsm_lchan *lchan, uint32_t fn, bool is_amr_sid_update)
{
	uint32_t fn104 = fn % 104;

	/* See TS 45.008 Sections 8.3 and 8.4 for a detailed descriptions of the rules
	 * implemented here. We only implement the logic for Voice, not CSD */

	switch (lchan->type) {
	case GSM_LCHAN_TCH_F:
		switch (lchan->tch_mode) {
		case GSM48_CMODE_SIGN:
		case GSM48_CMODE_SPEECH_V1:
		case GSM48_CMODE_SPEECH_EFR:
			if (trx_sched_is_sacch_fn(lchan->ts, fn, true, lchan->type))
				return true;
			if (ARRAY_CONTAINS(ts45008_83_tch_f, fn104))
				return true;
			break;
		case GSM48_CMODE_SPEECH_AMR:
			if (trx_sched_is_sacch_fn(lchan->ts, fn, true, lchan->type))
				return true;
			if (is_amr_sid_update)
				return true;
			break;
		default:
			LOGPFN(DMEAS, LOGL_ERROR, fn, "%s: Unsupported lchan->tch_mode %u\n",
				gsm_lchan_name(lchan), lchan->tch_mode);
			break;
		}
		break;
	case GSM_LCHAN_TCH_H:
		switch (lchan->tch_mode) {
		case GSM48_CMODE_SPEECH_V1:
			if (trx_sched_is_sacch_fn(lchan->ts, fn, true, lchan->type))
				return true;
			switch (lchan->nr) {
			case 0:
				if (ARRAY_CONTAINS(ts45008_83_tch_hs0, fn104))
					return true;
				break;
			case 1:
				if (ARRAY_CONTAINS(ts45008_83_tch_hs1, fn104))
					return true;
				break;
			default:
				OSMO_ASSERT(0);
			}
			break;
		case GSM48_CMODE_SPEECH_AMR:
			if (trx_sched_is_sacch_fn(lchan->ts, fn, true, lchan->type))
				return true;
			if (is_amr_sid_update)
				return true;
			break;
		case GSM48_CMODE_SIGN:
			/* No DTX allowed; SUB=FULL, therefore measurements at all frame numbers are
			 * SUB */
			return true;
		default:
			LOGPFN(DMEAS, LOGL_ERROR, fn, "%s: Unsupported lchan->tch_mode %u\n",
				gsm_lchan_name(lchan), lchan->tch_mode);
			break;
		}
		break;
	case GSM_LCHAN_SDCCH:
		/* No DTX allowed; SUB=FULL, therefore measurements at all frame numbers are SUB */
		return true;
	default:
		break;
	}
	return false;
}

/* Measurement reporting period and mapping of SACCH message block for TCHF
 * and TCHH chan As per in 3GPP TS 45.008, section 8.4.1.
 *
 *             Timeslot number (TN)        TDMA frame number (FN) modulo 104
 *             Half rate,    Half rate,     Reporting    SACCH
 * Full Rate   subch.0       subch.1        period       Message block
 * 0           0 and 1                      0 to 103     12,  38,  64,  90
 * 1                         0 and 1        13 to 12     25,  51,  77,  103
 * 2           2 and 3                      26 to 25     38,  64,  90,  12
 * 3                         2 and 3        39 to 38     51,  77,  103, 25
 * 4           4 and 5                      52 to 51     64,  90,  12,  38
 * 5                         4 and 5        65 to 64     77,  103, 25,  51
 * 6           6 and 7                      78 to 77     90,  12,  38,  64
 * 7                         6 and 7        91 to 90     103, 25,  51,  77 */

static const uint8_t tchf_meas_rep_fn104[] = {
	[0] =	90,
	[1] =	103,
	[2] =	12,
	[3] =	25,
	[4] =	38,
	[5] =	51,
	[6] =	64,
	[7] =	77,
};
static const uint8_t tchh0_meas_rep_fn104[] = {
	[0] =	90,
	[1] =	90,
	[2] =	12,
	[3] =	12,
	[4] =	38,
	[5] =	38,
	[6] =	64,
	[7] =	64,
};
static const uint8_t tchh1_meas_rep_fn104[] = {
	[0] =	103,
	[1] =	103,
	[2] =	25,
	[3] =	25,
	[4] =	51,
	[5] =	51,
	[6] =	77,
	[7] =	77,
};

/* Measurement reporting period for SDCCH8 and SDCCH4 chan
 * As per in 3GPP TS 45.008, section 8.4.2.
 *
 * Logical Chan		TDMA frame number
 *			(FN) modulo 102
 *
 * SDCCH/8		12 to 11
 * SDCCH/4		37 to 36
 */

/* FN of the first burst whose block completes before reaching fn%102=11 */
static const uint8_t sdcch8_meas_rep_fn102[] = {
	[0] = 66,	/* 15(SDCCH), 47(SACCH), 66(SDCCH) */
	[1] = 70,	/* 19(SDCCH), 51(SACCH), 70(SDCCH) */
	[2] = 74,	/* 23(SDCCH), 55(SACCH), 74(SDCCH) */
	[3] = 78,	/* 27(SDCCH), 59(SACCH), 78(SDCCH) */
	[4] = 98,	/* 31(SDCCH), 98(SACCH), 82(SDCCH) */
	[5] = 0,	/* 35(SDCCH),  0(SACCH), 86(SDCCH) */
	[6] = 4,	/* 39(SDCCH),  4(SACCH), 90(SDCCH) */
	[7] = 8,	/* 43(SDCCH),  8(SACCH), 94(SDCCH) */
};

/* FN of the first burst whose block completes before reaching fn%102=37 */
static const uint8_t sdcch4_meas_rep_fn102[] = {
	[0] = 88,	/* 37(SDCCH), 57(SACCH), 88(SDCCH) */
	[1] = 92,	/* 41(SDCCH), 61(SACCH), 92(SDCCH) */
	[2] = 6,	/*  6(SACCH), 47(SDCCH), 98(SDCCH) */
	[3] = 10	/* 10(SACCH),  0(SDCCH), 51(SDCCH) */
};

/* Note: The reporting of the measurement results is done via the SACCH channel.
 * The measurement interval is not aligned with the interval in which the
 * SACCH is transmitted. When we receive the measurement indication with the
 * SACCH block, the corresponding measurement interval will already have ended
 * and we will get the results late, but on spot with the beginning of the
 * next measurement interval.
 *
 * For example: We get a measurement indication on FN%104=38 in TS=2. Then we
 * will have to look at 3GPP TS 45.008, section 8.4.1 (or 3GPP TS 05.02 Clause 7
 * Table 1 of 9) what value we need to feed into the lookup tables in order to
 * detect the measurement period ending. In this example the "real" ending
 * was on FN%104=12. This is the value we have to look for in
 * tchf_meas_rep_fn104 to know that a measurement period has just ended. */

/* See also 3GPP TS 05.02 Clause 7 Table 1 of 9:
 * Mapping of logical channels onto physical channels (see subclauses 6.3, 6.4, 6.5) */
static uint8_t translate_tch_meas_rep_fn104(uint8_t fn_mod)
{
	switch (fn_mod) {
	case 25:
		return 103;
	case 38:
		return 12;
	case 51:
		return 25;
	case 64:
		return 38;
	case 77:
		return 51;
	case 90:
		return 64;
	case 103:
		return 77;
	case 12:
		return 90;
	}

	/* Invalid / not of interest */
	return 0;
}

/* determine if a measurement period ends at the given frame number */
static int is_meas_complete(struct gsm_lchan *lchan, uint32_t fn)
{
	unsigned int fn_mod = -1;
	const uint8_t *tbl;
	int rc = 0;
	enum gsm_phys_chan_config pchan = ts_pchan(lchan->ts);

	if (lchan->ts->nr >= 8)
		return -EINVAL;
	if (pchan >= _GSM_PCHAN_MAX)
		return -EINVAL;

	switch (pchan) {
	case GSM_PCHAN_TCH_F:
		fn_mod = translate_tch_meas_rep_fn104(fn % 104);
		if (tchf_meas_rep_fn104[lchan->ts->nr] == fn_mod)
			rc = 1;
		break;
	case GSM_PCHAN_TCH_H:
		fn_mod = translate_tch_meas_rep_fn104(fn % 104);
		if (lchan->nr == 0)
			tbl = tchh0_meas_rep_fn104;
		else
			tbl = tchh1_meas_rep_fn104;
		if (tbl[lchan->ts->nr] == fn_mod)
			rc = 1;
		break;
	case GSM_PCHAN_SDCCH8_SACCH8C:
	case GSM_PCHAN_SDCCH8_SACCH8C_CBCH:
		fn_mod = fn % 102;
		if (sdcch8_meas_rep_fn102[lchan->nr] == fn_mod)
			rc = 1;
		break;
	case GSM_PCHAN_CCCH_SDCCH4:
	case GSM_PCHAN_CCCH_SDCCH4_CBCH:
		fn_mod = fn % 102;
		if (sdcch4_meas_rep_fn102[lchan->nr] == fn_mod)
			rc = 1;
		break;
	default:
		rc = 0;
		break;
	}

	if (rc == 1) {
		DEBUGP(DMEAS,
		       "%s meas period end fn:%u, fn_mod:%i, status:%d, pchan:%s\n",
		       gsm_lchan_name(lchan), fn, fn_mod, rc, gsm_pchan_name(pchan));
	}

	return rc;
}

/* receive a L1 uplink measurement from L1 */
int lchan_new_ul_meas(struct gsm_lchan *lchan, struct bts_ul_meas *ulm, uint32_t fn)
{
	if (lchan->state != LCHAN_S_ACTIVE) {
		LOGPFN(DMEAS, LOGL_NOTICE, fn,
		     "%s measurement during state: %s, num_ul_meas=%d\n",
		     gsm_lchan_name(lchan), gsm_lchans_name(lchan->state),
		     lchan->meas.num_ul_meas);
	}

	if (lchan->meas.num_ul_meas >= ARRAY_SIZE(lchan->meas.uplink)) {
		LOGPFN(DMEAS, LOGL_NOTICE, fn,
		     "%s no space for uplink measurement, num_ul_meas=%d\n",
		     gsm_lchan_name(lchan), lchan->meas.num_ul_meas);
		return -ENOSPC;
	}

	/* We expect the lower layers to mark AMR SID_UPDATE frames already as such.
	 * In this function, we only deal with the comon logic as per the TS 45.008 tables */
	if (!ulm->is_sub)
		ulm->is_sub = ts45008_83_is_sub(lchan, fn, false);

	DEBUGPFN(DMEAS, fn, "%s adding measurement (is_sub=%u), num_ul_meas=%d\n",
		gsm_lchan_name(lchan), ulm->is_sub, lchan->meas.num_ul_meas);

	memcpy(&lchan->meas.uplink[lchan->meas.num_ul_meas++], ulm,
		sizeof(*ulm));

	return 0;
}

/* input: BER in steps of .01%, i.e. percent/100 */
static uint8_t ber10k_to_rxqual(uint32_t ber10k)
{
	/* Eight levels of Rx quality are defined and are mapped to the
	 * equivalent BER before channel decoding, as per in 3GPP TS 45.008,
	 * secton 8.2.4.
	 *
	 * RxQual:				BER Range:
	 * RXQUAL_0	     BER <  0,2 %       Assumed value = 0,14 %
	 * RXQUAL_1  0,2 % < BER <  0,4 %	Assumed value = 0,28 %
	 * RXQUAL_2  0,4 % < BER <  0,8 %	Assumed value = 0,57 %
	 * RXQUAL_3  0,8 % < BER <  1,6 %	Assumed value = 1,13 %
	 * RXQUAL_4  1,6 % < BER <  3,2 %	Assumed value = 2,26 %
	 * RXQUAL_5  3,2 % < BER <  6,4 %	Assumed value = 4,53 %
	 * RXQUAL_6  6,4 % < BER < 12,8 %	Assumed value = 9,05 %
	 * RXQUAL_7 12,8 % < BER		Assumed value = 18,10 % */

	if (ber10k < 20)
		return 0;
	if (ber10k < 40)
		return 1;
	if (ber10k < 80)
		return 2;
	if (ber10k < 160)
		return 3;
	if (ber10k < 320)
		return 4;
	if (ber10k < 640)
		return 5;
	if (ber10k < 1280)
		return 6;
	return 7;
}

int lchan_meas_check_compute(struct gsm_lchan *lchan, uint32_t fn)
{
	struct gsm_meas_rep_unidir *mru;
	uint32_t ber_full_sum = 0;
	uint32_t irssi_full_sum = 0;
	uint32_t ber_sub_sum = 0;
	uint32_t irssi_sub_sum = 0;
	int32_t ta256b_sum = 0;
	unsigned int num_meas_sub = 0;
	int i;

	/* if measurement period is not complete, abort */
	if (!is_meas_complete(lchan, fn))
		return 0;

	/* if there are no measurements, skip computation */
	if (lchan->meas.num_ul_meas == 0)
		return 0;

	/* compute the actual measurements */

	/* step 1: add up */
	for (i = 0; i < lchan->meas.num_ul_meas; i++) {
		struct bts_ul_meas *m = &lchan->meas.uplink[i];

		ber_full_sum += m->ber10k;
		irssi_full_sum += m->inv_rssi;
		ta256b_sum += m->ta_offs_256bits;

		if (m->is_sub) {
			num_meas_sub++;
			ber_sub_sum += m->ber10k;
			irssi_sub_sum += m->inv_rssi;
		}
	}

	/* step 2: divide */
	ber_full_sum = ber_full_sum / lchan->meas.num_ul_meas;
	irssi_full_sum = irssi_full_sum / lchan->meas.num_ul_meas;
	ta256b_sum = ta256b_sum / lchan->meas.num_ul_meas;

	if (num_meas_sub) {
		ber_sub_sum = ber_sub_sum / num_meas_sub;
		irssi_sub_sum = irssi_sub_sum / num_meas_sub;
	} else {
		LOGP(DMEAS, LOGL_ERROR, "%s No measurements for SUB!!!\n", gsm_lchan_name(lchan));
		/* The only situation in which this can occur is if the related uplink burst/block was
		 * missing, so let's set BER to 100% and level to lowest possible. */
		ber_sub_sum = 10000; /* 100% */
		irssi_sub_sum = 120; /* -120 dBm */
	}

	LOGP(DMEAS, LOGL_INFO, "%s Computed TA256(% 4d) BER-FULL(%2u.%02u%%), RSSI-FULL(-%3udBm), "
		"BER-SUB(%2u.%02u%%), RSSI-SUB(-%3udBm)\n", gsm_lchan_name(lchan),
		ta256b_sum, ber_full_sum/100,
		ber_full_sum%100, irssi_full_sum, ber_sub_sum/100, ber_sub_sum%100,
		irssi_sub_sum);

	/* store results */
	mru = &lchan->meas.ul_res;
	mru->full.rx_lev = dbm2rxlev((int)irssi_full_sum * -1);
	mru->sub.rx_lev = dbm2rxlev((int)irssi_sub_sum * -1);
	mru->full.rx_qual = ber10k_to_rxqual(ber_full_sum);
	mru->sub.rx_qual = ber10k_to_rxqual(ber_sub_sum);
	lchan->meas.ms_toa256 = ta256b_sum;

	LOGP(DMEAS, LOGL_INFO, "%s UL MEAS RXLEV_FULL(%u), RXLEV_SUB(%u),"
	       "RXQUAL_FULL(%u), RXQUAL_SUB(%u), num_meas_sub(%u), num_ul_meas(%u) \n",
	       gsm_lchan_name(lchan),
	       mru->full.rx_lev,
	       mru->sub.rx_lev,
	       mru->full.rx_qual,
	       mru->sub.rx_qual, num_meas_sub, lchan->meas.num_ul_meas);

	lchan->meas.flags |= LC_UL_M_F_RES_VALID;
	lchan->meas.num_ul_meas = 0;

	/* send a signal indicating computation is complete */

	return 1;
}

static int ts_meas_check_compute(struct gsm_bts_trx_ts *ts, uint32_t fn)
{
	int i;
	const int num_subslots = ts_subslots(ts);

	for (i = 0; i < num_subslots; ++i) {
		struct gsm_lchan *lchan = &ts->lchan[i];

		if (lchan->state != LCHAN_S_ACTIVE)
			continue;

		switch (lchan->type) {
		case GSM_LCHAN_SDCCH:
		case GSM_LCHAN_TCH_F:
		case GSM_LCHAN_TCH_H:
		case GSM_LCHAN_PDTCH:
			lchan_meas_check_compute(lchan, fn);
			break;
		default:
			break;
		}
	}
	return 0;
}

/* needs to be called once every TDMA frame ! */
int trx_meas_check_compute(struct gsm_bts_trx *trx, uint32_t fn)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(trx->ts); i++) {
		struct gsm_bts_trx_ts *ts = &trx->ts[i];
		ts_meas_check_compute(ts, fn);
	}
	return 0;
}

void meas_preproc_rep_init(struct gsm_lchan *lchan) {
	int i;

	/* clear cached measurement reports */
	LOGP(DMEAS, LOGL_DEBUG, "%s Clear measurement report\n", gsm_lchan_name(lchan));

	lchan->meas_preproc.meas_idx = 0;
	lchan->meas_preproc.meas_ave_idx = 0;
	lchan->meas.flags &= ~LC_AVE_F_RES_VALID;
	lchan->meas.flags &= ~LC_AVE_F_CACHE_VALID;
	lchan->meas.flags &= ~LC_HO_F_DIR_RETR_VALID;
	memset(lchan->meas_preproc.ul_ave_vec, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.ul_ave_res, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.dl_ave_vec, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.dl_ave_res, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.ul_qual_ave_vec, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.ul_qual_ave_res, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.dl_qual_ave_vec, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.dl_qual_ave_res, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.ms_bts_ave_vec, 0, MAX_NUM_MEAS_PREPROC);
	memset(lchan->meas_preproc.ms_bts_ave_res, 0, MAX_NUM_MEAS_PREPROC);

	for (i = 0; i < ARRAY_SIZE(lchan->meas_preproc.cell); i++){
		lchan->meas_preproc.cell[i].bcch_freq = 0;
		lchan->meas_preproc.cell[i].bsic = 0;
		lchan->meas_preproc.cell[i].rxlev = 0;
		memset(lchan->meas_preproc.cell[i].rxlev_vec, 0, MAX_NUM_MEAS_PREPROC);
		memset(lchan->meas_preproc.cell[i].rxlev_ave_res, 0, MAX_NUM_MEAS_PREPROC);
	}

	/* clear HO cause lists */
	lchan->meas_preproc.rec_ho_causes = 0;
	lchan->meas_preproc.cur_ho_causes = 0;

	/* Cancel any pending timer */
	osmo_timer_del(&lchan->meas_preproc.preproc_ho_timer);

	/* clear candidate cell in NCELL list */
	lchan->meas_preproc.candidate_ncell = 0;
}


/* Calculate HO cause decision */
static int meas_ho_cause_decision(uint8_t *res_vec, int cur_idx, int p, int n, uint8_t res_thres, int sign) {
	int i, j, k;
	uint8_t vote = 0;

	LOGP(DMEAS, LOGL_DEBUG, "Cached res_vec P=%d out of N=%d, thres=%x, buf=%s \n", p, n, res_thres, osmo_hexdump(res_vec, n));

	j = cur_idx - n + 1;
	if (j < 0)
		j =  n + j;

	LOGP(DMEAS, LOGL_DEBUG, "cur_idx=%d, start_idx=%d\n", cur_idx, j);

	for(i = j; i < j + n; i++) {
		k = i % n;
		/*if there is a measurement result higher threshold, we increase HO detection counter*/
		if ((int)res_vec[k] * sign > (int)res_thres * sign) {
			vote++;
			LOGP(DMEAS, LOGL_DEBUG, "HO decision detected res=%x, thres=%x, vote=%d \n", res_vec[k], res_thres, vote);
		}
	}
	/* if HO detection counter bigger that p_out_of_n, we will return valid HO cause generation */
	if  (vote < p)
		return 0;

	return vote;
}

/* Update recorded HO cause list */
int meas_update_record_ho_causes(struct gsm_lchan *lchan)
{
	int i;
	int tx_rep_pending = 0;

	for(i = 0; i < HO_RQD_CAUSE_MAX; i++) {
		/* evaluate valid HO cause only */
		if (lchan->meas_preproc.cur_ho_causes & (1 << i)) {
			/* check if HO cause has not already in recorded list */
			if ((lchan->meas_preproc.cur_ho_causes & (1 << i)) == (lchan->meas_preproc.rec_ho_causes & (1 << i)))
				continue;

			/* not in the recorded list, update recorded list now */
			lchan->meas_preproc.rec_ho_causes |=  (1 << i);
			tx_rep_pending++;
			LOGP(DMEAS, LOGL_DEBUG, "%s IPAC HO_CAUSE pending %d: recorded=0x%x, current=0x%x\n",
				gsm_lchan_name(lchan),
				tx_rep_pending,
				lchan->meas_preproc.rec_ho_causes,
				lchan->meas_preproc.cur_ho_causes);

		}
	}
	return tx_rep_pending;
}

/* find better cell */
int meas_better_cell(struct gsm_lchan *lchan)
{
	struct ipac_preproc_cfg *preproc_cfg = &lchan->ts->trx->trx_preproc_cfg;
	int num_cell = lchan->meas_preproc.num_cell;
	int rc, i;
	uint8_t better_rxlev = 0;
	int power_budget;

	/* make sure average result is valid */
	if (!(lchan->meas.flags & LC_AVE_F_RES_VALID))
		return -ENODATA;

	/* make sure average result is valid */
	if (!(lchan->meas.flags & LC_AVE_F_CACHE_VALID))
		return -ENODATA;

	/* print log for testing purpose */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXLEV) {
		LOGP(DMEAS, LOGL_DEBUG, "%s MEAS_PREPROC_AVG SERVICING CELL rxlev_ul_avg=%3ddBm, rxlev_dl_avg=%3ddBm\n",
			gsm_lchan_name(lchan),
			rxlev2dbm(lchan->meas_preproc.ul_ave_res[lchan->meas_preproc.meas_ave_idx]),
			rxlev2dbm(lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx]));
	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXQUAL) {
		LOGP(DMEAS, LOGL_DEBUG, "%s MEAS_PREPROC_AVG SERVICING CELL rxqual_ul_avg=%d, rxqual_dl_avg=%d\n",
			gsm_lchan_name(lchan),
			lchan->meas_preproc.ul_qual_ave_res[lchan->meas_preproc.meas_ave_idx],
			lchan->meas_preproc.dl_qual_ave_res[lchan->meas_preproc.meas_ave_idx]);
	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_DIST) {
		LOGP(DMEAS, LOGL_DEBUG, "%s MEAS_PREPROC_AVG SERVICING CELL ms_ta_avg=%d\n",
			gsm_lchan_name(lchan),
			lchan->meas_preproc.ms_bts_ave_vec[lchan->meas_preproc.meas_ave_idx]);

	}
	/* FIXME : we do nothing if there is no neighbor CELL reported */
	if(!num_cell)
		return -ENODATA;

	for (i = 0; i < num_cell; i++) {
		LOGP(DMEAS, LOGL_DEBUG, "%s MEAS_PREPROC_AVG NCELL(%d) rxlev_avg=%3ddBm\n",
			gsm_lchan_name(lchan),
			i,
			rxlev2dbm(lchan->meas_preproc.cell[i].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx]));
	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXLEV) {
		LOGP(DMEAS, LOGL_DEBUG, "%s Cached ul_ave_res %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ul_ave_res, preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt),
				lchan->meas_preproc.meas_ave_idx);

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached dl_ave_res %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.dl_ave_res, preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt),
				lchan->meas_preproc.meas_ave_idx);

		for (i = 0; i < num_cell; i++) {
			LOGP(DMEAS, LOGL_DEBUG, "%s Cached ncell(%d) rxlev_avg_res %s, idx=%d\n",
					gsm_lchan_name(lchan),
					i,
					osmo_hexdump(lchan->meas_preproc.cell[i].rxlev_ave_res, preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt),
					lchan->meas_preproc.meas_ave_idx);
		}
	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXQUAL) {
		LOGP(DMEAS, LOGL_DEBUG, "%s Cached ul_qual_ave_res %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ul_qual_ave_res, preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqt),
				lchan->meas_preproc.meas_ave_idx);

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached dl_qual_ave_res %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.dl_qual_ave_res, preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqt),
				lchan->meas_preproc.meas_ave_idx);
	}

	/* find the best neighbor cell if possible */
	for (i = 0; i < num_cell; i++) {
		if ( lchan->meas_preproc.cell[i].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx] > better_rxlev ) {
			better_rxlev = lchan->meas_preproc.cell[i].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx];
			lchan->meas_preproc.better_ncell = i;
		}
	}

	LOGP(DMEAS, LOGL_DEBUG, "%s Better NCELL(%d), BSIC=%d, BCCH-FREQ=%d, RXLEV=%3ddBm\n", gsm_lchan_name(lchan),
			lchan->meas_preproc.better_ncell,
			lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bsic,
			lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bcch_freq,
			rxlev2dbm(better_rxlev));

	return 0;
}

/* lookup better cell in preconfigued NCELL list */
int meas_lookup_ncell_list(struct gsm_lchan *lchan)
{
	int i;
	struct ipac_preproc_cfg *preproc_cfg = &lchan->ts->trx->trx_preproc_cfg;
	int num_ncell_cfg = preproc_cfg->num_ncell_cfg;
	uint8_t meas_better_cell = lchan->meas_preproc.better_ncell;
	uint8_t meas_better_cell_bsic = lchan->meas_preproc.cell[meas_better_cell].bsic;
	uint8_t meas_better_cell_bcch_freq = lchan->meas_preproc.cell[meas_better_cell].bcch_freq;
	uint8_t meas_better_rxlev = lchan->meas_preproc.cell[meas_better_cell].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx];;
	int candidate_ncell = -EPERM;

	/* make sure the NCELL list is available */
	if (!num_ncell_cfg)
		return -EINVAL;

	while (num_ncell_cfg--) {
		if (meas_better_cell_bsic == preproc_cfg->ncell_list[num_ncell_cfg].bsic &&
			meas_better_cell_bcch_freq == preproc_cfg->ncell_list[num_ncell_cfg].bcch_freq1 &&
			meas_better_cell_bcch_freq == preproc_cfg->ncell_list[num_ncell_cfg].bcch_freq2) {
			/* found candidate NCELL in preconfigured list */
			candidate_ncell = num_ncell_cfg;
			LOGP(DMEAS, LOGL_DEBUG, "%s Found better cell: BSIC=%d, BCCH-FREQ=%d, RXLEV=%3ddBm in NCELL_LIST(%d): RXLEV_MIN=%3ddBm, HO_MARGIN=%d\n", gsm_lchan_name(lchan),
					meas_better_cell_bsic, meas_better_cell_bcch_freq, rxlev2dbm(meas_better_rxlev),
					candidate_ncell, rxlev2dbm(preproc_cfg->ncell_list[candidate_ncell].rxlev_min),
					preproc_cfg->ncell_list[candidate_ncell].ho_margin);
			break;
		}
	}
	/* return candidate cell found in NCELL list */
	lchan->meas_preproc.candidate_ncell = candidate_ncell;
	return 0;
}

/* Update HO causes */
int meas_update_ho_causes(struct gsm_lchan *lchan)
{
	struct ipac_preproc_cfg *preproc_cfg = &lchan->ts->trx->trx_preproc_cfg;
	int rc, i;
	uint8_t better_rxlev = 0;
	uint8_t rxlev_min = 0;
	uint8_t rxlev_serv = 0;
	int power_budget;
	uint8_t ho_margin = 0;
	int candidate_ncell = lchan->meas_preproc.candidate_ncell;

	/* do not evaluate cause with invalid candidate NCELL */
	if (candidate_ncell < 0)
		return -EINVAL;

	/* get better NCELL averaged RXLEV */
	better_rxlev = lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx];

	/* Use minimum RXLEV found in the preconfigured NCELL list */
	rxlev_min = preproc_cfg->ncell_list[candidate_ncell].rxlev_min;

	/* Use HO margin found in the preconfigured NCELL list */
	ho_margin = preproc_cfg->ncell_list[candidate_ncell].ho_margin;

	if (better_rxlev <= (rxlev_min + ho_margin)) {
		LOGP(DMEAS, LOGL_NOTICE, "%s HO: Better NCELL(%d), BSIC=%d, BCCH-FREQ=%d, RXLEV=%3ddBm < RXLEV_MIN=%3ddBm + HO_MARGIN=%3dB threshold to continue\n", gsm_lchan_name(lchan),
					lchan->meas_preproc.better_ncell,
					lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bsic,
					lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bcch_freq,
					rxlev2dbm(better_rxlev), rxlev2dbm(rxlev_min), ho_margin);
		lchan->meas_preproc.cur_ho_causes = 0;
		return 0;
	}

	/* RXLEV DL servicing cell */
	rxlev_serv = lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx];
	if (better_rxlev <= (rxlev_serv + ho_margin)) {
		LOGP(DMEAS, LOGL_NOTICE, "%s HO: Better NCELL(%d), BSIC=%d, BCCH-FREQ=%d, RXLEV=%3ddBm < RXLEV_SERV=%3ddBm + HO_MARGIN=%3dB threshold to continue\n", gsm_lchan_name(lchan),
					lchan->meas_preproc.better_ncell,
					lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bsic,
					lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].bcch_freq,
					rxlev2dbm(better_rxlev), rxlev2dbm(rxlev_serv), ho_margin);
		lchan->meas_preproc.cur_ho_causes = 0;
		return 0;
	}

	/* update HO cause based on averaged RXLEV */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXLEV) {
		/* GSM 05.08 section A.3.2.2a */
		/* check for RXLEV UL*/
		rc = meas_ho_cause_decision(lchan->meas_preproc.ul_ave_res,
					lchan->meas_preproc.meas_ave_idx,
					preproc_cfg->ho_comp.p5,
					preproc_cfg->ho_comp.n5,
					preproc_cfg->ho_thresh.l_rxlev_ul_h,
					-1);
		if (rc) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_L_RXLEV_UL_H;

			LOGP(DMEAS, LOGL_NOTICE, "%s HO_RQD_CAUSE_L_RXLEV_UL_H detected rxlev=%3ddBm, l_rxlev_ul_h=%3ddBm \n",
				gsm_lchan_name(lchan),
				rxlev2dbm(lchan->meas_preproc.ul_ave_res[lchan->meas_preproc.meas_ave_idx]),
				rxlev2dbm(preproc_cfg->ho_thresh.l_rxlev_ul_h));

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_L_RXLEV_UL_H;

		/* check for RXLEV DL */
		rc = meas_ho_cause_decision(lchan->meas_preproc.dl_ave_res,
								lchan->meas_preproc.meas_ave_idx,
								preproc_cfg->ho_comp.p5,
								preproc_cfg->ho_comp.n5,
								preproc_cfg->ho_thresh.l_rxlev_dl_h,
								-1);
		if (rc) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_L_RXLEV_DL_H;

			LOGP(DMEAS, LOGL_NOTICE, "%s %s detected rxlev=%3ddBm, l_rxlev_ul_h=%3ddBm \n",
				gsm_lchan_name(lchan),
				get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_L_RXLEV_DL_H),
				rxlev2dbm(lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx]),
				rxlev2dbm(preproc_cfg->ho_thresh.l_rxlev_dl_h));

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_L_RXLEV_DL_H;

		/* GSM 05.08 section A.3.2.2c */
		/* check for RXQUAL UL */
		rc = meas_ho_cause_decision(lchan->meas_preproc.ul_qual_ave_res,
					lchan->meas_preproc.meas_ave_idx,
					preproc_cfg->ho_comp.p6,
					preproc_cfg->ho_comp.n6,
					preproc_cfg->ho_thresh.l_rxqual_ul_h,
					1);
		if (rc) {
			/* check for RXLEV UL */
			rc = meas_ho_cause_decision(lchan->meas_preproc.ul_ave_res,
						lchan->meas_preproc.meas_ave_idx,
						preproc_cfg->ho_comp.p7,
						preproc_cfg->ho_comp.n7,
						preproc_cfg->ho_thresh.rxlev_ul_ih,
						1);
			if (rc) {
				lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_RXLEV_UL_IH;

				LOGP(DMEAS, LOGL_NOTICE, "%s %s detected rxlev=%3ddBm, l_rxlev_ul_h=%3ddBm \n",
					gsm_lchan_name(lchan),
					get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_RXLEV_UL_IH),
					rxlev2dbm(lchan->meas_preproc.ul_ave_res[lchan->meas_preproc.meas_ave_idx]),
					rxlev2dbm(preproc_cfg->ho_thresh.rxlev_ul_ih));
			}

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_RXLEV_UL_IH;

		/* check for RXQUAL DL */
		rc = meas_ho_cause_decision(lchan->meas_preproc.dl_qual_ave_res,
					lchan->meas_preproc.meas_ave_idx,
					preproc_cfg->ho_comp.p6,
					preproc_cfg->ho_comp.n6,
					preproc_cfg->ho_thresh.l_rxqual_dl_h,
					1);
		if (rc) {
			/* check for RXLEV DL */
			rc = meas_ho_cause_decision(lchan->meas_preproc.dl_ave_res,
						lchan->meas_preproc.meas_ave_idx,
						preproc_cfg->ho_comp.p7,
						preproc_cfg->ho_comp.n7,
						preproc_cfg->ho_thresh.rxlev_dl_ih,
						1);
			if (rc) {
				lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_RXLEV_DL_IH;

				LOGP(DMEAS, LOGL_NOTICE, "%s %s detected rxlev=%3ddBm, l_rxlev_ul_h=%3ddBm \n",
					gsm_lchan_name(lchan),
					get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_RXLEV_DL_IH),
					rxlev2dbm(lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx]),
					rxlev2dbm(preproc_cfg->ho_thresh.rxlev_dl_ih));
			}

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_RXLEV_DL_IH;

		/* GSM 05.08 section A.3.2.2e */
		power_budget = better_rxlev - rxlev_serv;

		if (power_budget > ho_margin) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_POWER_BUDGET;

			LOGP(DMEAS, LOGL_NOTICE, "%s NCELL(%d) %s detected RXLEV=%3ddBm, NCELL-RXLEV=%3ddBm, HO_MARGIN=%d, PBGT=%ddB\n",
				gsm_lchan_name(lchan),
				lchan->meas_preproc.better_ncell,
				get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_POWER_BUDGET),
				rxlev2dbm(lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx]),
				rxlev2dbm(lchan->meas_preproc.cell[lchan->meas_preproc.better_ncell].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx]),
				ho_margin, power_budget);

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_POWER_BUDGET;

	}

	/* update HO cause based on averaged RXQUAL */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXQUAL) {
		/* GSM 05.08 section A.3.2.2b */
		rc = meas_ho_cause_decision(lchan->meas_preproc.ul_qual_ave_res,
					lchan->meas_preproc.meas_ave_idx,
					preproc_cfg->ho_comp.p6,
					preproc_cfg->ho_comp.n6,
					preproc_cfg->ho_thresh.l_rxqual_ul_h,
					1);

		if (rc) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_L_RXQUAL_UL_H;

			LOGP(DMEAS, LOGL_NOTICE, "%s %s detected rxqual=%d, l_rxqual_ul_h=%d\n",
				gsm_lchan_name(lchan),
				get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_L_RXQUAL_UL_H),
				lchan->meas_preproc.ul_qual_ave_res[lchan->meas_preproc.meas_ave_idx],
				preproc_cfg->ho_thresh.l_rxqual_ul_h);

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_L_RXQUAL_UL_H;

		rc = meas_ho_cause_decision(lchan->meas_preproc.dl_qual_ave_res,
			lchan->meas_preproc.meas_ave_idx,
			preproc_cfg->ho_comp.p6,
			preproc_cfg->ho_comp.n6,
			preproc_cfg->ho_thresh.l_rxqual_dl_h,
			1);

		if (rc) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_L_RXQUAL_DL_H;

			LOGP(DMEAS, LOGL_NOTICE, "%s %s detected rxqual=%d, l_rxqual_dl_h=%d \n",
				gsm_lchan_name(lchan),
				get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_L_RXQUAL_DL_H),
				lchan->meas_preproc.dl_qual_ave_res[lchan->meas_preproc.meas_ave_idx],
				preproc_cfg->ho_thresh.l_rxqual_dl_h);

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_L_RXQUAL_DL_H;

	}

	/* update HO cause based on averaged MS-BTS distance */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_DIST) {
		/* GSM 05.08 section 3.2.2d */
		/*validate threshold for the maximum distance between MS and current BTS for HO process to start */
		rc = meas_ho_cause_decision(lchan->meas_preproc.ms_bts_ave_res,
					lchan->meas_preproc.meas_ave_idx,
					preproc_cfg->ho_comp.p8,
					preproc_cfg->ho_comp.n8,
					preproc_cfg->ho_thresh.ms_range_max,
					1);

		if (rc) {
			lchan->meas_preproc.cur_ho_causes |= HO_RQD_CAUSE_MAX_MS_RANGE;

			LOGP(DMEAS, LOGL_NOTICE, "%s %s detected ms_ta=%d, ms_range_max=%d \n",
				gsm_lchan_name(lchan),
				get_value_string(rsl_ipac_preproc_meas_ho_cause_strs, IPAC_HO_RQD_CAUSE_MAX_MS_RANGE),
				lchan->meas_preproc.ms_bts_ave_res[lchan->meas_preproc.meas_ave_idx],
				preproc_cfg->ho_thresh.ms_range_max);

		} else
			lchan->meas_preproc.cur_ho_causes &= ~HO_RQD_CAUSE_MAX_MS_RANGE;
	}

	LOGP(DMEAS, LOGL_DEBUG, "%s IPAC HO_CAUSES: recorded=0x%x, current=0x%x\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.rec_ho_causes,
		lchan->meas_preproc.cur_ho_causes);

	/* nothing to do when no current HO cause */
	if (!lchan->meas_preproc.cur_ho_causes)
		return 0;

	/* only add new HO cause to recorded HO cause list */
	rc = meas_update_record_ho_causes(lchan);

	return rc;
}

/* Parse MS measurement report */
int meas_parse_ms_rep(struct msgb *msg, struct gsm_lchan *lchan)
{
	int i;
	struct gsm48_hdr *gh = msgb_l3(msg);
	uint8_t *l3_info = gh->data;
	struct bitvec *bv;

	lchan->meas_preproc.ms_pwr = ms_pwr_dbm(msg->trx->bts->band, lchan->meas.l1_info[0] >> 3);
	lchan->meas_preproc.ms_ta = lchan->meas.l1_info[1] >> 1;

	/* UL measurement */
	LOGP(DMEAS, LOGL_DEBUG, "%s L3 INFO hexdump: %s\n",
			gsm_lchan_name(lchan),
			osmo_hexdump(msgb_l3(msg),
			msgb_l3len(msg)));

	/* check if measurement is valid */
	if ( (l3_info[1] & 0x40) ) {
		return -1;
	}

	/* set DL MES_REP valid flag */
	lchan->meas.flags |= LC_DL_M_F_RES_VALID;

	/* BA_USED flag */
	if ( (l3_info[0] & 0x80) )
			lchan->meas.flags |= LC_NCELL_M_F_BA1;

	/* UL DTX flag */
	if ( (l3_info[0] & 0x40) )
		lchan->meas.flags |= LC_UL_M_F_DTX;

	LOGP(DMEAS, LOGL_DEBUG, "%s L3 INFO MEAS FLAGS 0x%02x\n",
			gsm_lchan_name(lchan),
			lchan->meas.flags);

	bv = talloc_zero(tall_bts_ctx, struct bitvec);

	bv->data_len = msgb_l3len(msg);
	bv->data = l3_info;
	bv->cur_bit = 2;

	lchan->meas_preproc.dl_res.full.rx_lev = bitvec_get_uint(bv, 6) & 0x3f;
	bv->cur_bit += 2;

	lchan->meas_preproc.dl_res.sub.rx_lev = bitvec_get_uint(bv, 6) & 0x3f;
	bv->cur_bit += 1;

	lchan->meas_preproc.dl_res.full.rx_qual = bitvec_get_uint(bv, 3) & 0x7;
	lchan->meas_preproc.dl_res.sub.rx_qual = bitvec_get_uint(bv, 3) & 0x7;

	/* neighbor cells */
	lchan->meas_preproc.num_cell = bitvec_get_uint(bv, 3) & 0x07;
	if ( lchan->meas_preproc.num_cell == 0 ) {
		for (i = 0; i < ARRAY_SIZE(lchan->meas_preproc.cell); i++){
			lchan->meas_preproc.cell[i].bcch_freq = 0;
			lchan->meas_preproc.cell[i].bsic = 0;
			lchan->meas_preproc.cell[i].rxlev = 0;
			memset(lchan->meas_preproc.cell[i].rxlev_vec, 0, MAX_NUM_MEAS_PREPROC);
			memset(lchan->meas_preproc.cell[i].rxlev_ave_res, 0, MAX_NUM_MEAS_PREPROC);
		}
	}

	/* NCELL info */
	for (i = 0 ; i < lchan->meas_preproc.num_cell; i++) {
		lchan->meas_preproc.cell[i].rxlev = bitvec_get_uint(bv, 6) & 0x3f;
		lchan->meas_preproc.cell[i].bcch_freq = bitvec_get_uint(bv, 5) & 0x1f;
		lchan->meas_preproc.cell[i].bsic = bitvec_get_uint(bv, 6) & 0x3f;

		LOGP(DMEAS, LOGL_DEBUG, "%s NCELL(%d) RXL-NCELL=%3ddBm BCCH-FREQ-NCELL=%d BSIC-NCELL=%d\n",
			gsm_lchan_name(lchan),
			i,
			rxlev2dbm(lchan->meas_preproc.cell[i].rxlev),
			lchan->meas_preproc.cell[i].bcch_freq,
			lchan->meas_preproc.cell[i].bsic);
	}
	/* copy UL measurement results */
	lchan->meas_preproc.ul_res = lchan->meas.ul_res;

	LOGP(DMEAS, LOGL_DEBUG, "%s RXL-FULL-UL=%3ddBm RXL-SUB-UL=%3ddBm\n",
		gsm_lchan_name(lchan),
		rxlev2dbm(lchan->meas_preproc.ul_res.full.rx_lev),
		rxlev2dbm(lchan->meas_preproc.ul_res.sub.rx_lev));

	LOGP(DMEAS, LOGL_DEBUG, "%s RXQ-FULL-UL=%d RXQ-SUB-UL=%d\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.ul_res.full.rx_qual,
		lchan->meas_preproc.ul_res.sub.rx_qual);

	LOGP(DMEAS, LOGL_DEBUG, "%s MS L1 TX PWR=%3ddBm MS L1 TA=%u\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.ms_pwr,
		lchan->meas_preproc.ms_ta);

	LOGP(DMEAS, LOGL_DEBUG, "%s RXL-FULL-DL=%3ddBm RXL-SUB-DL=%3ddBm\n",
		gsm_lchan_name(lchan),
		rxlev2dbm(lchan->meas_preproc.dl_res.full.rx_lev),
		rxlev2dbm(lchan->meas_preproc.dl_res.sub.rx_lev));

	LOGP(DMEAS, LOGL_DEBUG, "%s RXQ-FULL-DL=%d RXQ-SUB-DL=%d\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.dl_res.full.rx_qual,
		lchan->meas_preproc.dl_res.sub.rx_qual);

	/* free bit vector buffer */
	talloc_free(bv);

	return 0;
}

static int meas_preproc_unweighted_avg(int avg_len, uint8_t *in_buf) {
	int i;
	int avg_res = 0;

	for (i = 0; i < avg_len; i++) {
		avg_res += in_buf[i];
	}
	return (avg_res/avg_len);
}

int meas_preproc_avg(struct gsm_lchan *lchan)
{
	struct ipac_preproc_cfg *preproc_cfg = &lchan->ts->trx->trx_preproc_cfg;
	int i;
	int ul_avg = 0;
	int dl_avg = 0;
	int ul_qual_avg = 0;
	int dl_qual_avg = 0;
	int ms_bts_avg = 0;
	int ncell_avg[6] = {0, 0, 0, 0, 0, 0};
	uint8_t num_cell = lchan->meas_preproc.num_cell;
	uint8_t meas_cache_len = preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqave;

	/* STEP1: cache measurement results */
	LOGP(DMEAS, LOGL_DEBUG, "%s Caching input MS MEAS_REP (%d/%d)\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.meas_idx,
		meas_cache_len);

	/* cache RXLEV */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXLEV) {
		LOGP(DMEAS, LOGL_DEBUG, "%s RSL_IPAC_EIE_MEAS_AVG_CFG param_id=0x%02x, h_reqave=0x%02x, avg_method=0x%02x, h_reqt=0x%02x \n",
			gsm_lchan_name(lchan),
			preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].param_id,
			preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqave,
			preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].ave_method,
			preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt);

		lchan->meas_preproc.ul_ave_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.ul_res.sub.rx_lev;
		lchan->meas_preproc.dl_ave_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.dl_res.sub.rx_lev;

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached input ul_ave_vec %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ul_ave_vec, meas_cache_len),
				lchan->meas_preproc.meas_idx);

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached input dl_ave_vec %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ul_ave_vec, meas_cache_len),
				lchan->meas_preproc.meas_idx);
	}

	/* cache RXQUAL */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXQUAL) {
		LOGP(DMEAS, LOGL_DEBUG, "%s RSL_IPAC_EIE_MEAS_AVG_CFG param_id=0x%02x, h_reqave=0x%02x, avg_method=0x%02x, h_reqt=0x%02x \n",
			gsm_lchan_name(lchan),
			preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].param_id,
			preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqave,
			preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].ave_method,
			preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqt);

		lchan->meas_preproc.ul_qual_ave_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.ul_res.sub.rx_qual;
		lchan->meas_preproc.dl_qual_ave_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.dl_res.sub.rx_qual;

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached input ul_qual_ave_vec %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ul_qual_ave_vec, meas_cache_len),
				lchan->meas_preproc.meas_idx);

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached input dl_qual_ave_vec %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.dl_qual_ave_vec, meas_cache_len),
				lchan->meas_preproc.meas_idx);
	}

	/* cache MS-BTS distance */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_DIST) {
		LOGP(DMEAS, LOGL_DEBUG, "%s RSL_IPAC_EIE_MEAS_AVG_CFG param_id=0x%02x, h_reqave=0x%02x, avg_method=0x%02x, h_reqt=0x%02x \n",
			gsm_lchan_name(lchan),
			preproc_cfg->ms_ave_cfg[IPAC_MS_BTS_DIS_AVE].param_id,
			preproc_cfg->ms_ave_cfg[IPAC_MS_BTS_DIS_AVE].h_reqave,
			preproc_cfg->ms_ave_cfg[IPAC_MS_BTS_DIS_AVE].ave_method,
			preproc_cfg->ms_ave_cfg[IPAC_MS_BTS_DIS_AVE].h_reqt);

		lchan->meas_preproc.ms_bts_ave_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.ms_ta;

		LOGP(DMEAS, LOGL_DEBUG, "%s Cached input ms_bts_ave_vec %s, idx=%d\n",
				gsm_lchan_name(lchan),
				osmo_hexdump(lchan->meas_preproc.ms_bts_ave_vec, meas_cache_len),
				lchan->meas_preproc.meas_idx);
	}

	/* cache neighbor cells RXLEV measurement */
	for (i = 0; i < num_cell; i++) {
		lchan->meas_preproc.cell[i].rxlev_vec[lchan->meas_preproc.meas_idx] = lchan->meas_preproc.cell[i].rxlev;
	}

	/* set measurement valid flag */
	if (lchan->meas_preproc.meas_idx == meas_cache_len - 1 && !(lchan->meas.flags & LC_AVE_F_RES_VALID ))
		lchan->meas.flags |= LC_AVE_F_RES_VALID;

	/* rollover measurement index of input vectors*/
	lchan->meas_preproc.meas_idx = (lchan->meas_preproc.meas_idx + 1) % meas_cache_len;

	/* make sure we cache enough measurement results before averaging */
	if (!(lchan->meas.flags & LC_AVE_F_RES_VALID))
		return -EINPROGRESS;

	/* STEP 2: Averaging process. We need to maintain h_reqt averaged values prior to perform HO decision */
	LOGP(DMEAS, LOGL_DEBUG, "%s Caching MS MEAS_AVE_REP (%d/%d)\n",
		gsm_lchan_name(lchan),
		lchan->meas_preproc.meas_ave_idx,
		preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt);

	/* servicing cell averaging */
	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXLEV) {
		/* averaging RXLEV */
		ul_avg = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqave, lchan->meas_preproc.ul_ave_vec);
		dl_avg = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqave, lchan->meas_preproc.dl_ave_vec);

		/* return averaged RXLEV results */
		lchan->meas_preproc.ul_ave_res[lchan->meas_preproc.meas_ave_idx] = ul_avg;
		lchan->meas_preproc.dl_ave_res[lchan->meas_preproc.meas_ave_idx] = dl_avg;

	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_RXQUAL) {
		/* averaging RXQUAL */
		ul_qual_avg = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqave, lchan->meas_preproc.ul_qual_ave_vec);
		dl_qual_avg = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_RXQUAL_AVE].h_reqave, lchan->meas_preproc.dl_qual_ave_vec);

		/* return averaged RXQUAL results */
		lchan->meas_preproc.ul_qual_ave_res[lchan->meas_preproc.meas_ave_idx] = ul_qual_avg;
		lchan->meas_preproc.dl_qual_ave_res[lchan->meas_preproc.meas_ave_idx] = dl_qual_avg;
	}

	if (preproc_cfg->meas_mode_flags & MEAS_PREPROC_AVG_PARAMID_DIST) {
		/* averaging MS-BTS distance */
		ms_bts_avg = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_MS_BTS_DIS_AVE].h_reqave, lchan->meas_preproc.ms_bts_ave_vec);

		/* return averaged MS-BTS distance results */
		lchan->meas_preproc.ms_bts_ave_res[lchan->meas_preproc.meas_ave_idx] = ms_bts_avg;
	}

	/* neighbor cell RXLEV averaging */
	for (i = 0; i < num_cell; i++) {
		/* averaging neighbor cell RXLEV */
		ncell_avg[i] = meas_preproc_unweighted_avg(preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqave, lchan->meas_preproc.cell[i].rxlev_vec);

		/* return neighbor cell averaged RXLEV result */
		lchan->meas_preproc.cell[i].rxlev_ave_res[lchan->meas_preproc.meas_ave_idx] = ncell_avg[i];
	}

	/* set average valid flag */
	if ((lchan->meas_preproc.meas_ave_idx == preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt - 1) && !(lchan->meas.flags & LC_AVE_F_CACHE_VALID ))
		lchan->meas.flags |= LC_AVE_F_CACHE_VALID;

	/* roll-over measurement average results pointer */
	lchan->meas_preproc.meas_ave_idx = (lchan->meas_preproc.meas_ave_idx + 1) % (preproc_cfg->ms_ave_cfg[IPAC_RXLEV_AVE].h_reqt);

	/* make sure we cache enough averaged measurement results */
	if (!(lchan->meas.flags & LC_AVE_F_CACHE_VALID))
		return -EINPROGRESS;

	return 0;
}
